import typing
import unittest
import subprocess
import threading
from time import sleep
import os
import datetime

os.environ["PYTHONUNBUFFERED"] = 'yes'

driver = ('driver1@jabber.lqdn.fr', 'qwerty')
passenger = ('passenger1@jabber.lqdn.fr', 'qwerty')
passenger_2 = ('passenger2@jabber.lqdn.fr', 'qwerty')

LOGDIR = 'logs/{}/'.format(datetime.datetime.now().strftime('%m.%d.%Y_%H:%M:%S'))
os.makedirs(LOGDIR)
START_TIME = datetime.datetime.now()


class ProcessRunner():
    def __init__(self, cmd, log_file: typing.Optional[typing.TextIO] = None):
        self._cmd = cmd
        self._proc = None
        self._reading_thread = None
        self._recording = False
        self._record_buffer = []
        self._log_file = log_file

    def start(self):
        self._proc = subprocess.Popen(
            self._cmd.split(' '), stdout=subprocess.PIPE, shell=False, encoding='utf8', bufsize=0)
        self._reading_thread = threading.Thread(target=self.loglines)
        self._reading_thread.start()

    def loglines(self):
        while True:
            l = self._proc.stdout.readline()
            if not l:
                self._proc.stdout.close()
                break
            if self._log_file:
                self._log_file.writelines(
                    [str(int((datetime.datetime.now() - START_TIME).total_seconds() * 1000)) + ': ' + l])
            if self._recording:
                self._record_buffer.append(l)

    def start_recording(self):
        self._record_buffer = []
        self._recording = True

    def stop_recording(self):
        self._recording = False
        return ''.join(self._record_buffer)

    def stop(self):
        self._proc.kill()
        self._proc.wait()
        self._reading_thread.join()


class TestAll(unittest.TestCase):

    def test_driver_launch(self):

        driver1 = ProcessRunner(
            'runner driver -n DriverBob -s bobsock -e {} -p {}'.format(driver[0], driver[1]))
        driver1.start_recording()
        driver1.start()
        sleep(2)
        res = driver1.stop_recording()
        for s in ['Agent driver1 created', 'Waiting for user offer', 'Waiting for XMPP message']:
            self.assertIn(s, res)

        controller1 = ProcessRunner('controller -s bobsock driver ping -s 10')
        driver1.start_recording()
        controller1.start_recording()
        controller1.start()
        sleep(2)
        res = driver1.stop_recording()
        for s in ['Received message from controller', 'ping_res', 'ping_req', 'seq']:
            self.assertIn(s, res)
        res = controller1.stop_recording()
        for s in ['Received message:', 'ping_res', 'ping_req', 'seq']:
            self.assertIn(s, res)

        driver1.stop()
        controller1.stop()

    def test_passenger_launch(self):

        driver1 = ProcessRunner(
            'runner passenger -n PassengerAlice -s alicesock -e {} -p {}'.format(passenger[0], passenger[1]))
        driver1.start_recording()
        driver1.start()
        sleep(4)
        res = driver1.stop_recording()
        for s in ['Agent passenger1 created', 'Waiting for user request', 'Waiting for XMPP message']:
            self.assertIn(s, res)

        controller1 = ProcessRunner(
            'controller -s alicesock passenger ping -s 10')
        driver1.start_recording()
        controller1.start_recording()
        controller1.start()
        sleep(2)
        res = driver1.stop_recording()
        for s in ['Received message from controller', 'ping_res', 'ping_req', 'seq']:
            self.assertIn(s, res)
        res = controller1.stop_recording()
        for s in ['Received message:', 'ping_res', 'ping_req', 'seq']:
            self.assertIn(s, res)

        driver1.stop()
        controller1.stop()

    def test_add_offer(self):

        driver1 = ProcessRunner(
            'runner driver -n DriverBob -s bobsock -e {} -p {}'.format(driver[0], driver[1]))
        driver1.start_recording()
        driver1.start()

        passenger1 = ProcessRunner(
            'runner passenger -n PassengerAlice -s alicesock -e {} -p {}'.format(passenger[0], passenger[1]))
        passenger1.start_recording()
        passenger1.start()

        sleep(2)

        controller1 = ProcessRunner(
            'controller -s bobsock driver add_offer -f 1,2 -t 6,6 -m 4 -d 2021_01_10_10_35 -o 99')
        controller1.start_recording()
        controller1.start()

        sleep(2)

        res = controller1.stop_recording()
        for s in ['user_offer_req', 'user_offer_res']:
            self.assertIn(s, res)

        res = driver1.stop_recording()
        for s in ['New User Offer', 'Sending msg:', 'Sent.']:
            self.assertIn(s, res)

        res = passenger1.stop_recording()
        # print(res)  # Tutaj można sobie podejrzeć samemu najpierw, co wypluł w tym przypadku pasażer i na podstawie tego napisać asercję
        for s in ['Received XMPP message', 'Received offer']:
            self.assertIn(s, res)

        driver1.stop()
        passenger1.stop()
        controller1.stop()

    def test_driver_launch_immediate_ping(self):

        driver1 = ProcessRunner(
            'runner driver -n DriverBob -s bobsock -e {} -p {}'.format(driver[0], driver[1]))
        driver1.start_recording()
        controller1 = ProcessRunner('controller -s bobsock driver ping -s 10')
        controller1.start_recording()
        # Start in reverse order
        controller1.start()
        sleep(1)
        driver1.start()
        sleep(3)
        res = driver1.stop_recording()
        for s in ['Received message from controller', 'ping_res', 'ping_req', 'seq']:
            self.assertIn(s, res)
        res = controller1.stop_recording()
        for s in ['Received message:', 'ping_res', 'ping_req', 'seq']:
            self.assertIn(s, res)

        driver1.stop()
        controller1.stop()

    def test_add_request(self):

        passenger1 = ProcessRunner(
            'runner passenger -n PassengerAlice -s alicesock -e {} -p {}'.format(passenger[0], passenger[1]))
        passenger1.start_recording()
        passenger1.start()

        sleep(1)

        controller1 = ProcessRunner(
            'controller -s alicesock passenger add_request -f 1,2 -t 6,6 -d 2021_02_10_10_00')
        controller1.start_recording()
        controller1.start()

        sleep(1)

        controller2 = ProcessRunner(
            'controller -s alicesock passenger show_requests')
        controller2.start_recording()
        controller2.start()

        sleep(1)

        res = passenger1.stop_recording()
        for s in ['user_request_req', 'user_request_res', 'New User Request']:
            self.assertIn(s, res)

        res = controller1.stop_recording()
        for s in ['Received message', 'user_request_res']:
            self.assertIn(s, res)

        res = controller2.stop_recording()
        for s in ['Received message', 'requests_status_res', 'status: FREE', 'index: 0']:
            self.assertIn(s, res)

        passenger1.stop()
        controller1.stop()
        controller2.stop()

    def test_request_offer_match(self):

        passenger1 = ProcessRunner(
            'runner passenger -n PassengerAlice -s alicesock -e {} -p {}'.format(passenger[0], passenger[1]))
        passenger1.start_recording()
        passenger1.start()

        driver1 = ProcessRunner(
            'runner driver -n DriverBob -s bobsock -e {} -p {}'.format(driver[0], driver[1]))
        driver1.start_recording()
        driver1.start()

        sleep(1)

        controller1 = ProcessRunner(
            'controller -s alicesock passenger add_request -f 1,4 -t 6,9 -d 2022_02_10_10_10')
        controller1.start_recording()
        controller1.start()

        sleep(1)

        controller2 = ProcessRunner(
            'controller -s bobsock driver add_offer -f 1,2 -t 6,6 -m 4 -d 2022_02_10_10_20 -o 5')
        controller2.start_recording()
        controller2.start()

        sleep(3)

        controller3 = ProcessRunner(
            'controller -s bobsock driver show_offers')
        controller3.start_recording()
        controller3.start()

        sleep(1)

        controller4 = ProcessRunner(
            'controller -s alicesock passenger show_requests')
        controller4.start_recording()
        controller4.start()

        sleep(1)

        res = passenger1.stop_recording()
        for s in ['Received confirmation']:
            self.assertIn(s, res)

        res = driver1.stop_recording()
        for s in ['Received declaration message', '"confirmation": "yes"']:
            self.assertIn(s, res)

        res = controller1.stop_recording()
        for s in ['user_request_res']:
            self.assertIn(s, res)

        res = controller2.stop_recording()
        for s in ['user_offer_res']:
            self.assertIn(s, res)

        res = controller3.stop_recording()
        for s in ['places: 3', 'name: "']:
            self.assertIn(s, res)

        res = controller4.stop_recording()
        for s in ['status: ACCEPTED', 'name: "']:
            self.assertIn(s, res)

        passenger1.stop()
        driver1.stop()
        controller1.stop()
        controller2.stop()
        controller3.stop()
        controller4.stop()

    def test_request_offer_no_places(self):

        passenger1 = ProcessRunner(
            'runner passenger -n PassengerAlice -s alicesock -e {} -p {}'.format(passenger[0], passenger[1]))
        passenger1.start_recording()
        passenger1.start()

        driver1 = ProcessRunner(
            'runner driver -n DriverBob -s bobsock -e {} -p {}'.format(driver[0], driver[1]))
        driver1.start_recording()
        driver1.start()

        sleep(1)

        controller1 = ProcessRunner(
            'controller -s alicesock passenger add_request -f 1,4 -t 6,9 -d 2022_02_10_10_10')
        controller1.start_recording()
        controller1.start()

        sleep(1)

        controller2 = ProcessRunner(
            'controller -s bobsock driver add_offer -f 1,2 -t 6,6 -m 0 -d 2022_02_10_10_20 -o 5')
        controller2.start_recording()
        controller2.start()

        sleep(3)

        controller3 = ProcessRunner(
            'controller -s bobsock driver show_offers')
        controller3.start_recording()
        controller3.start()

        sleep(1)

        controller4 = ProcessRunner(
            'controller -s alicesock passenger show_requests')
        controller4.start_recording()
        controller4.start()

        sleep(1)

        res = passenger1.stop_recording()
        for s in ['Received confirmation']:
            self.assertIn(s, res)

        res = driver1.stop_recording()
        for s in ['Received declaration message', '"confirmation": "no"']:
            self.assertIn(s, res)

        res = controller1.stop_recording()
        for s in ['user_request_res']:
            self.assertIn(s, res)

        res = controller2.stop_recording()
        for s in ['user_offer_res']:
            self.assertIn(s, res)

        res = controller3.stop_recording()
        for s in ['places: 0']:
            self.assertIn(s, res)

        res = controller4.stop_recording()
        for s in ['status: FREE']:
            self.assertIn(s, res)

        passenger1.stop()
        driver1.stop()
        controller1.stop()
        controller2.stop()
        controller3.stop()
        controller4.stop()

    def test_offer_cancel(self):

        passenger1 = ProcessRunner(
            'runner passenger -n PassengerAlice -s alicesock -e {} -p {}'.format(passenger[0], passenger[1]))
        passenger1.start_recording()
        passenger1.start()

        driver1 = ProcessRunner(
            'runner driver -n DriverBob -s bobsock -e {} -p {}'.format(driver[0], driver[1]))
        driver1.start_recording()
        driver1.start()

        sleep(1)

        controller1 = ProcessRunner(
            'controller -s alicesock passenger add_request -f 1,4 -t 6,9 -d 2022_02_10_10_10')
        controller1.start_recording()
        controller1.start()

        sleep(1)

        controller2 = ProcessRunner(
            'controller -s bobsock driver add_offer -f 1,2 -t 6,6 -m 4 -d 2022_02_10_10_20 -o 5')
        controller2.start_recording()
        controller2.start()

        sleep(3)

        controller3 = ProcessRunner(
            'controller -s bobsock driver show_offers')
        controller3.start_recording()
        controller3.start()

        sleep(1)

        controller4 = ProcessRunner(
            'controller -s alicesock passenger show_requests')
        controller4.start_recording()
        controller4.start()

        sleep(1)

        controller5 = ProcessRunner(
            'controller -s bobsock driver cancel_offer -i 0')
        controller5.start_recording()
        controller5.start()

        sleep(1)

        controller6 = ProcessRunner(
            'controller -s bobsock driver show_offers')
        controller6.start_recording()
        controller6.start()

        sleep(1)

        controller7 = ProcessRunner(
            'controller -s alicesock passenger show_requests')
        controller7.start_recording()
        controller7.start()

        sleep(1)

        res = passenger1.stop_recording()
        for s in ['Received confirmation', 'Received cancellation offer']:
            self.assertIn(s, res)

        res = driver1.stop_recording()
        for s in ['Received declaration message', '"confirmation": "yes"', 'New User Cancellation', '"msg_type": "cancel"']:
            self.assertIn(s, res)

        res = controller1.stop_recording()
        for s in ['user_request_res']:
            self.assertIn(s, res)

        res = controller2.stop_recording()
        for s in ['user_offer_res']:
            self.assertIn(s, res)

        res = controller3.stop_recording()
        for s in ['places: 3', 'name: "']:
            self.assertIn(s, res)

        res = controller4.stop_recording()
        for s in ['status: ACCEPTED', 'name: "']:
            self.assertIn(s, res)

        res = controller5.stop_recording()
        for s in ['cancel_offer_res']:
            self.assertIn(s, res)

        res = controller6.stop_recording()
        for s in ['Received message:\noffers_status_res {\n}']:
            self.assertIn(s, res)

        res = controller7.stop_recording()
        for s in ['status: FREE']:
            self.assertIn(s, res)

        passenger1.stop()
        driver1.stop()
        controller1.stop()
        controller2.stop()
        controller3.stop()
        controller4.stop()
        controller5.stop()
        controller6.stop()
        controller7.stop()

    def test_declaration_cancel(self):

        passenger1 = ProcessRunner(
            'runner passenger -n PassengerAlice -s alicesock -e {} -p {}'.format(passenger[0], passenger[1]))
        passenger1.start_recording()
        passenger1.start()

        driver1 = ProcessRunner(
            'runner driver -n DriverBob -s bobsock -e {} -p {}'.format(driver[0], driver[1]))
        driver1.start_recording()
        driver1.start()

        sleep(1)

        controller1 = ProcessRunner(
            'controller -s alicesock passenger add_request -f 1,4 -t 6,9 -d 2022_02_10_10_10')
        controller1.start_recording()
        controller1.start()

        sleep(1)

        controller2 = ProcessRunner(
            'controller -s bobsock driver add_offer -f 1,2 -t 6,6 -m 4 -d 2022_02_10_10_20 -o 5')
        controller2.start_recording()
        controller2.start()

        sleep(3)

        controller3 = ProcessRunner(
            'controller -s bobsock driver show_offers')
        controller3.start_recording()
        controller3.start()

        sleep(1)

        controller4 = ProcessRunner(
            'controller -s alicesock passenger show_requests')
        controller4.start_recording()
        controller4.start()

        sleep(1)

        controller5 = ProcessRunner(
            'controller -s alicesock passenger cancel_request -i 0')
        controller5.start_recording()
        controller5.start()

        sleep(1)

        controller6 = ProcessRunner(
            'controller -s bobsock driver show_offers')
        controller6.start_recording()
        controller6.start()

        sleep(1)

        controller7 = ProcessRunner(
            'controller -s alicesock passenger show_requests')
        controller7.start_recording()
        controller7.start()

        sleep(1)

        res = passenger1.stop_recording()
        for s in ['Received confirmation', 'New User Cancel Request', 'requests_status_res {\n}']:
            self.assertIn(s, res)

        res = driver1.stop_recording()
        for s in ['Received declaration message', '"confirmation": "yes"', 'Received cancel request message']:
            self.assertIn(s, res)

        res = controller1.stop_recording()
        for s in ['user_request_res']:
            self.assertIn(s, res)

        res = controller2.stop_recording()
        for s in ['user_offer_res']:
            self.assertIn(s, res)

        res = controller3.stop_recording()
        for s in ['places: 3', 'name: "']:
            self.assertIn(s, res)

        res = controller4.stop_recording()
        for s in ['status: ACCEPTED', 'name: "']:
            self.assertIn(s, res)

        res = controller5.stop_recording()
        for s in ['cancel_request_res']:
            self.assertIn(s, res)

        res = controller6.stop_recording()
        for s in ['places: 4']:
            self.assertIn(s, res)

        res = controller7.stop_recording()
        for s in ['requests_status_res {\n}']:
            self.assertIn(s, res)

        passenger1.stop()
        driver1.stop()
        controller1.stop()
        controller2.stop()
        controller3.stop()
        controller4.stop()
        controller5.stop()
        controller6.stop()
        controller7.stop()

    def test_full(self):

        p1file = open(LOGDIR + 'p1.txt', 'a')
        p2file = open(LOGDIR + 'p2.txt', 'a')
        d1file = open(LOGDIR + 'd1.txt', 'a')

        cp1file = open(LOGDIR + 'cp1.txt', 'a')
        cp2file = open(LOGDIR + 'cp2.txt', 'a')
        cd1file = open(LOGDIR + 'cd1.txt', 'a')

        passenger1 = ProcessRunner(
            'runner passenger -n PassengerAlice -s alicesock -e {} -p {}'.format(passenger[0], passenger[1]), p1file)
        passenger1.start_recording()
        passenger1.start()

        passenger2 = ProcessRunner(
            'runner passenger -n PassengerCharlie -s charliesock -e {} -p {}'.format(passenger_2[0], passenger_2[1]), p2file)
        passenger2.start_recording()
        passenger2.start()

        driver1 = ProcessRunner(
            'runner driver -n DriverBob -s bobsock -e {} -p {}'.format(driver[0], driver[1]), d1file)
        driver1.start_recording()
        driver1.start()

        sleep(3)

        controller1 = ProcessRunner(
            'controller -s alicesock passenger add_request -f 1,4 -t 6,9 -d 2022_02_10_10_10', cp1file)
        controller1.start_recording()
        controller1.start()

        controller2 = ProcessRunner(
            'controller -s charliesock passenger add_request -f 1,4 -t 6,9 -d 2022_02_10_10_10', cp2file)
        controller2.start_recording()
        controller2.start()

        sleep(1)

        controller3 = ProcessRunner(
            'controller -s bobsock driver add_offer -f 1,2 -t 6,6 -m 1 -d 2022_02_10_10_20 -o 5', cd1file)
        controller3.start_recording()
        controller3.start()

        sleep(3)

        controller4 = ProcessRunner(
            'controller -s bobsock driver show_offers', cd1file)
        controller4.start_recording()
        controller4.start()

        controller5 = ProcessRunner(
            'controller -s alicesock passenger show_requests', cp1file)
        controller5.start_recording()
        controller5.start()

        controller6 = ProcessRunner(
            'controller -s charliesock passenger show_requests', cp2file)
        controller6.start_recording()
        controller6.start()

        sleep(1)

        res = passenger1.stop_recording()
        for s in ['"confirmation": "yes"']:
            self.assertIn(s, res)

        res = passenger2.stop_recording()
        for s in ['"confirmation": "no"']:
            self.assertIn(s, res)

        res = driver1.stop_recording()
        for s in []:
            self.assertIn(s, res)

        res = controller1.stop_recording()
        for s in []:
            self.assertIn(s, res)

        res = controller2.stop_recording()
        for s in []:
            self.assertIn(s, res)

        res = controller3.stop_recording()
        for s in []:
            self.assertIn(s, res)

        res = controller4.stop_recording()
        for s in ['places: 0', 'name:']:
            self.assertIn(s, res)

        res = controller5.stop_recording()
        for s in ['status: ACCEPTED']:
            self.assertIn(s, res)

        res = controller6.stop_recording()
        for s in ['status: FREE']:
            self.assertIn(s, res)

        controller1.stop()
        controller2.stop()
        controller3.stop()
        controller4.stop()
        controller5.stop()
        controller6.stop()

        # Passenger cancellation
        driver1.start_recording()
        passenger1.start_recording()
        passenger2.start_recording()

        controller7 = ProcessRunner(
            'controller -s alicesock passenger cancel_request -i 0', cp1file)
        controller7.start_recording()
        controller7.start()

        sleep(2)

        controller8 = ProcessRunner(
            'controller -s alicesock passenger show_requests', cp1file)
        controller8.start_recording()
        controller8.start()

        controller9 = ProcessRunner(
            'controller -s charliesock passenger show_requests', cp2file)
        controller9.start_recording()
        controller9.start()

        sleep(1)

        res = controller7.stop_recording()
        for s in ['cancel_request_res']:
            self.assertIn(s, res)

        res = controller8.stop_recording()
        for s in ['requests_status_res {\n}']:
            self.assertIn(s, res)

        res = controller9.stop_recording()
        for s in ['status: ACCEPTED']:
            self.assertIn(s, res)

        controller7.stop()
        controller8.stop()
        controller9.stop()

        # Driver cancellation

        controller10 = ProcessRunner(
            'controller -s bobsock driver cancel_offer -i 0', cd1file)
        controller10.start_recording()
        controller10.start()

        sleep(2)

        controller11 = ProcessRunner(
            'controller -s charliesock passenger show_requests', cp2file)
        controller11.start_recording()
        controller11.start()

        sleep(1)

        res = controller10.stop_recording()
        for s in ['cancel_offer_res']:
            self.assertIn(s, res)

        res = controller11.stop_recording()
        for s in ['status: FREE']:
            self.assertIn(s, res)

        controller10.stop()
        controller11.stop()

        passenger1.stop()
        passenger2.stop()
        driver1.stop()

        p1file.close()
        p2file.close()
        d1file.close()
        cp1file.close()
        cp2file.close()
        cd1file.close()
