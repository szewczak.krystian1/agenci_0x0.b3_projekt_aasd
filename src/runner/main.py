import argparse
from runner.agents.driver_agent import DriverAgent
from runner.agents.passenger_agent import PassengerAgent
from time import sleep


def main():
    print("Running agent runner")
    p = make_parser()
    args = p.parse_args()
    if not args.subparser:
        p.print_help()
        exit(1)
    if args.subparser == 'driver':
        print('Launching DRIVER agent ' + args.name)
        with DriverAgent(args.socket, args.email, args.password) as d:
            future = d.start()
            future.result()
            while True:
                try:
                    sleep(1)
                except KeyboardInterrupt:
                    print('Stopping')
                    break
            d.stop()
    elif args.subparser == 'passenger':
        print('Launching PASSENGER agent ' + args.name)
        with PassengerAgent(args.socket, args.email, args.password) as d:
            future = d.start()
            future.result()
            while True:
                try:
                    sleep(1)
                except KeyboardInterrupt:
                    print('Stopping')
                    break
            d.stop()


def make_parser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(
        dest='subparser', help='Type of agent to launch')

    driver_parser = subparsers.add_parser(name='driver', help='Driver agent')
    driver_parser.add_argument(
        '-n', '--name', dest='name', type=str, required=True, help='Name of agent')
    driver_parser.add_argument(
        '-s', '--socket', dest='socket', type=str, required=True, help='Address of socket to create for agent')
    driver_parser.add_argument(
        '-e', '--email', dest='email', type=str, required=True, help='Email for login to XMPP')
    driver_parser.add_argument(
        '-p', '--password', dest='password', type=str, required=True, help='Password for login to XMPP')

    passenger_parser = subparsers.add_parser(
        name='passenger', help='Passenger agent')
    passenger_parser.add_argument(
        '-n', '--name', dest='name', type=str, required=True, help='Name of agent')
    passenger_parser.add_argument(
        '-s', '--socket', dest='socket', type=str, required=True, help='Address of socket to create for agent')
    passenger_parser.add_argument(
        '-e', '--email', dest='email', type=str, required=True, help='Email for login to XMPP')
    passenger_parser.add_argument(
        '-p', '--password', dest='password', type=str, required=True, help='Password for login to XMPP')
    return parser


if __name__ == '__main__':
    main()
