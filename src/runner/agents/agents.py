ALL_AGENTS = [
    'superjulek@hell.la',
    'superarek@hell.la',
    'superanna@hell.la',
    'superkrystian@hell.la',
    'kierowca1@jabber.uk',
    'passenger1@jabber.lqdn.fr',
    'passenger2@jabber.lqdn.fr',
    'working_driver@jabb.im',
    'driver1@jabber.lqdn.fr',
]

DRIVER_AGENTS = [
    'driver1@jabber.lqdn.fr',
]

PASSENGER_AGENTS = [
    'passenger1@jabber.lqdn.fr',
    'passenger2@jabber.lqdn.fr',
]
