from email.message import Message
from runner.agents.base_agent import BaseAgent
from runner.agents.agents import DRIVER_AGENTS, PASSENGER_AGENTS
from spade.behaviour import CyclicBehaviour
from spade import agent, message
import typing
import protos.msg_pb2 as pb
from asyncio import Queue
import asyncio
import datetime
import time
import json
import janus
from runner.agents.utils import convert_string_loc_to_xy
from threading import Lock


class DriverAgent(BaseAgent, agent.Agent):

    def __init__(self, sock_addr, email, password) -> None:
        agent.Agent.__init__(self, email, password)
        self._offer_checking_behav = _CheckUserOffersBehav()
        self._message_receiving_behav = _MessageReceivingBehav()
        self._declaration_checking_behav = _DeclarationHandlingBehav()
        self._cancel_request_handling_behav = _CancelPassengerRequestHandlingBehav()
        self._cancel_offer_checking_behav = _CheckUserCancelBehav()
        BaseAgent.__init__(self, sock_addr)

        self._offers: typing.List[InnerOffer] = []

    async def setup(self):
        print('Agent %s created' % (self.name))
        self.add_behaviour(self._offer_checking_behav)
        self.add_behaviour(self._message_receiving_behav)
        self.add_behaviour(self._declaration_checking_behav)
        self.add_behaviour(self._cancel_request_handling_behav)
        self.add_behaviour(self._cancel_offer_checking_behav)

    def _msg_type(self):
        return pb.DriverMsg

    def _handlers(self) -> typing.Dict[type, typing.Callable]:
        return {
            'ping_req': self._handle_ping_req,
            'user_offer_req': self._handle_user_offer_req,
            'offers_status_req': self._handle_offers_status_req,
            'cancel_offer_req': self._handle_cancel_offer_req,
        }

# HANDLERS

    def _handle_ping_req(self, event: pb.PingReq):
        self.reply_controller(pb.DriverMsg(ping_res=pb.PingRes(seq=event.seq)))

    def _handle_user_offer_req(self, event: pb.UserOfferReq):
        """Handled in separate behaviour cause could take long"""
        self._offer_checking_behav._start_lock.acquire()
        self._offer_checking_behav.offers_queue.sync_q.put(event.offer_info)
        self._offer_checking_behav._start_lock.release()
        self.reply_controller(pb.DriverMsg(user_offer_res=pb.UserOfferRes()))

    def _handle_offers_status_req(self, event: pb.OffersStatusReq):
        status_proto_l = []
        for o in self._offers:
            status_proto_l.append(o.to_status_proto())
        reply = pb.DriverMsg(
            offers_status_res=pb.OffersStatusRes(offers=status_proto_l))
        self.reply_controller(reply)

    def _handle_cancel_offer_req(self, event: pb.CancelOfferReq):
        self._cancel_offer_checking_behav._start_lock.acquire()
        self._cancel_offer_checking_behav.cancels_queue.sync_q.put(
            event.index
        )
        self._cancel_offer_checking_behav._start_lock.release()
        self.reply_controller(pb.DriverMsg(
            cancel_offer_res=pb.CancelOfferRes()
        ))


class _CheckUserOffersBehav(CyclicBehaviour):

    def __init__(self):
        super().__init__()
        self._start_lock = Lock()
        self._start_lock.acquire()

    async def on_start(self) -> None:
        # Trzeba użyć tej, żeby obsługiwać ją z części z asyncio i tej nie
        self.offers_queue = janus.Queue()
        self._start_lock.release()
        print('Starting User Offer Checking behaviour')

    async def run(self) -> None:
        print('Waiting for user offer')
        new_offer = await self.offers_queue.async_q.get()
        if new_offer is None:
            return
        print('New User Offer:\n' + str(new_offer))
        inner_offer = InnerOffer(new_offer)
        self.agent._offers.append(inner_offer)
        await self.spread_offer(inner_offer)

    async def on_end(self) -> None:
        print('Stopping User Offer Checking behaviour')

    async def spread_offer(self, offer: "InnerOffer"):
        for passenger_agent in PASSENGER_AGENTS:
            msg = message.Message(to=passenger_agent)
            msg.set_metadata('ontology', 'carpool')
            msg.set_metadata('perfomative', 'propose')
            # TODO: set rest of this ....
            m_body = offer.to_dict()
            m_body['driver'] = str(self.agent.jid)
            m_body['msg_type'] = 'offer'
            msg.body = json.dumps(m_body)
            print('Sending msg: ' + str(msg))
            try:
                await self.send(msg)
            except Exception as e:
                print('Error sending msg: ' + str(e))
            print('Sent.')


class _CheckUserCancelBehav(CyclicBehaviour):

    def __init__(self):
        super().__init__()
        self._start_lock = Lock()
        self._start_lock.acquire()

    async def on_start(self) -> None:
        self.cancels_queue = janus.Queue()
        self._start_lock.release()
        print('Starting User Cancel Offer Checking behaviour')

    async def run(self) -> None:
        print('Waiting for user offer cancellation')
        new_cancel = await self.cancels_queue.async_q.get()
        if new_cancel is None:
            return
        print('New User Cancellation:\n' + str(new_cancel))
        # Check for passengers
        passengers = await self.check_for_passengers(new_cancel)
        if passengers is None:
            print("There was not any passenger, offer_cancelled.")
            return
        # Send messages to passengers
        await self.spread_cancellation(passengers, new_cancel)

    async def on_end(self) -> None:
        print('Stopping User Cancel Offer Checking behaviour')

    async def check_for_passengers(self, offer_id: int):
        for offer in self.agent._offers:
            if offer.index == offer_id:
                offer_founded = offer
                break
            else:
                offer_founded = None
        if offer_founded is None:
            print("Such an offer does not exist in the system")
            return

        self.agent._offers.remove(offer_founded)
        return offer_founded.passengers

    async def spread_cancellation(self, passengers: "InnerPassenger", offer_id: int):
        for passenger_agent in passengers:
            msg = message.Message(to=passenger_agent.name)
            msg.set_metadata('ontology', 'carpool')
            msg.set_metadata('perfomative', 'cancel')
            m_body = {}
            m_body['msg_type'] = 'cancel'
            m_body['offer_id'] = offer_id
            m_body['request_id'] = passenger_agent.request_id
            msg.body = json.dumps(m_body)
            print('Sending msg: ' + str(msg))
            try:
                await self.send(msg)
            except Exception as e:
                print('Error sending msg: ' + str(e))
            print('Sent.')


class _MessageReceivingBehav(CyclicBehaviour):

    async def on_start(self) -> None:
        print('Starting XMPP Message receiving behaviour')

    async def run(self) -> None:
        print('Waiting for XMPP message')
        msg = await self.receive(timeout=60)
        if msg is None:
            print('No message received')
            return
        print('Received XMPP message: ' + str(msg))
        await self.handle_msg(msg)

    async def on_end(self) -> None:
        print('Stopping XMPP Message receiving behaviour')

    async def handle_msg(self, msg: message.Message):
        m_body = msg.body
        try:
            b_dict: dict = json.loads(m_body)
            t = b_dict['msg_type']
            if t == 'declaration':
                await self.agent._declaration_checking_behav.declarations_queue.put(msg)
            elif t == 'cancel':
                await self.agent._cancel_request_handling_behav.cancel_reuqest_queue.put(msg)
            else:
                raise Exception('Unknown message type')
        except Exception as e:
            print('Error parsing message: ' + str(e))


class _DeclarationHandlingBehav(CyclicBehaviour):

    async def on_start(self) -> None:
        print('Starting Declaration handling behaviour')
        self.declarations_queue: "Queue[message.Message]" = Queue(maxsize=1000)

    async def run(self) -> None:
        print('Waiting for declarations')
        declar_msg = await self.declarations_queue.get()
        if declar_msg is None:
            return
        print('Received declaration message: ' + str(declar_msg))
        offer = await self._check_inner_offers(declar_msg)
        if offer is None:
            return
        body = json.loads(declar_msg.body)
        print(body)
        passenger = InnerPassenger(
            str(declar_msg.sender), body['request_id'], body['from'], body['to'])
        await self._send_confirmation(passenger, offer)

    async def on_end(self) -> None:
        print('Stopping Declaration Message handling receiving behaviour')

    async def _check_inner_offers(self, declar_msg: message.Message):
        declar_offer_idx = json.loads(declar_msg.body)["offer_id"]
        for offer in self.agent._offers:
            if offer.index == declar_offer_idx:
                if _does_declaration_fit_to_offer(offer, declar_msg):
                    return offer
                break

    async def _send_confirmation(self, passenger: "InnerPassenger", offer: "InnerOffer"):
        msg = message.Message(to=passenger.name)
        msg.set_metadata('ontology', 'carpool')
        msg.set_metadata('perfomative', 'confirm')
        m_body = {}
        m_body['msg_type'] = 'confirmation'
        m_body['offer_id'] = offer.index
        m_body['request_id'] = passenger.request_id
        if offer.places > 0:
            m_body['confirmation'] = 'yes'
            offer.add_passenger(passenger)

        else:
            m_body['confirmation'] = 'no'

        msg.body = json.dumps(m_body)
        print('Sending msg: ' + str(msg))
        try:
            await self.send(msg)
        except Exception as e:
            print('Error sending msg: ' + str(e))
        print('Sent.')


class _CancelPassengerRequestHandlingBehav(CyclicBehaviour):

    async def on_start(self) -> None:
        print('Starting Passenger Request handling behaviour')
        self.cancel_reuqest_queue: "Queue[message.Message]" = Queue(
            maxsize=1000)

    async def run(self) -> None:
        print('Waiting for requests cancellations')
        cancel_msg = await self.cancel_reuqest_queue.get()
        if cancel_msg is None:
            return
        print('Received cancel request message: ' + str(cancel_msg))
        offer = await self.agent._declaration_checking_behav._check_inner_offers(cancel_msg)
        if offer is None:
            return
        await self._remove_passenger(offer, cancel_msg)

    async def _remove_passenger(self, offer: "InnerOffer", cancel_msg: Message):
        body = json.loads(cancel_msg.body)
        offer.remove_passenger(body['request_id'])
        await self.agent._offer_checking_behav.spread_offer(offer)

    async def on_end(self) -> None:
        print('Stopping Cancel Request Message handling receiving behaviour')


class InnerOffer:
    cnt = 0

    def __init__(self, proto: pb.OfferInfo):
        # Proto data
        self.from_x = proto.from_loc.x_coord
        self.from_y = proto.from_loc.y_coord
        self.to_x = proto.to_loc.x_coord
        self.to_y = proto.to_loc.y_coord
        self.date = datetime.datetime.fromtimestamp(proto.date)
        self.places = proto.places  # possible seats in the car
        self.max_offset = proto.max_offset
        # Business data
        self.index = self.cnt
        self.passengers: typing.List[InnerPassenger] = []
        InnerOffer.cnt += 1

    def to_status_proto(self) -> pb.OfferStatus:
        passengers_proto_l = []
        for p in self.passengers:
            passengers_proto_l.append(p.to_proto())

        offer_info_proto = pb.OfferStatus(
            offer_info=pb.OfferInfo(
                from_loc=pb.Location(
                    x_coord=self.from_x,
                    y_coord=self.from_y
                ),
                to_loc=pb.Location(
                    x_coord=self.to_x,
                    y_coord=self.to_y
                ),
                places=self.places,
                date=int(time.mktime(self.date.timetuple())),
                max_offset=self.max_offset
            ),
            passengers=passengers_proto_l,
            index=self.index
        )
        return offer_info_proto

    def to_dict(self):
        return {
            'from': str(self.from_x) + '.' + str(self.from_y),
            'to': str(self.to_x) + '.' + str(self.to_y),
            'date': str(int(time.mktime(self.date.timetuple()))),
            'offset': str(self.max_offset),
            'offer_id': self.index
        }

    def add_passenger(self, passenger: "InnerPassenger"):
        self.passengers.append(passenger)
        self.places -= 1

    def remove_passenger(self, request_id):
        for passenger in self.passengers:
            if passenger._request_id == request_id:
                to_delete = passenger
                break
        try:
            self.passengers.remove(to_delete)
            self.places += 1
        except:
            print("There is no such a request ID in passenger list")


class InnerPassenger:
    def __init__(self, name: str, request_id: int, loc_from="0.0", loc_to="0.0"):
        '''
            : param loc_from: Coordinates in string form "X.Y"
            : param loc_from: Coordinates in string form "X.Y"
        '''
        self._name = name
        self._request_id = request_id
        self.from_x, self.from_y = convert_string_loc_to_xy(loc_from)
        self.to_x, self.to_y = convert_string_loc_to_xy(loc_to)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name: str):
        self._name = name

    @property
    def request_id(self):
        return self._request_id

    @request_id.setter
    def request_id(self, id: int):
        self._request_id = id

    def to_proto(self):
        return pb.PassengerInfo(
            user=pb.UserInfo(name=self.name),
            from_loc=pb.Location(
                x_coord=self.from_x,
                y_coord=self.from_y
            ),
            to_loc=pb.Location(
                x_coord=self.to_x,
                y_coord=self.to_y
            )
        )


def _does_declaration_fit_to_offer(offer: "InnerOffer", declar_msg: message.Message):
    # Sprawdzenie czy na pewno deklaracaj odpowiada danej ofercie, tzn. czas + odleglosci
    # Aktualnie sprawdzane jest i tak po stronie pasażera, dlatego nie zdefiniowana
    # Jeśli wymagane jest policzenie tego, to wiadomość od pasażera powinna zawierać informacje takie jak
    # data wyjazdu + skąd / dokąd jedzie. Najlepiej dołożyć to w metodzie to_dict w klasie InnerRequest
    return True
