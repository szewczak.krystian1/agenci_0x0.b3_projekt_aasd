import math

def distance_from_line(a, b, c, point):
    """
    Function to calculate the distance from the straight line to specific point
    a,b,c are the parameters of general form equation of a line Ax + By + C = 0
    """
    x, y = point
    dist = abs(a * x + b * y + c) / math.sqrt(pow(a, 2) + pow(b, 2))
    return dist


def distance_from_route(start, end, pickup_point, destination):
    """
    Function to calculate the distance from the driver's route to the pickup point and the passenger's destination
    :param start: coordinates of the driver's starting point
    :param end: coordinates of the driver's  destination
    :param pickup_point: coordinates of the passenger's pickup point
    :param destination: coordinates of the passenger's destination
    :return: distance
    """

    # Calculate parameters of general form equation of a line Ax + By + C = 0
    start_x, start_y = start
    end_x, end_y = end
    a = start_y - end_y
    b = end_x - start_x
    c = (start_x - end_x) * start_y + (end_y - start_y) * start_x

    dist_pickup = distance_from_line(a, b, c, pickup_point)
    dist_destination = distance_from_line(a, b, c, destination)
    
    return dist_pickup + dist_destination

def convert_string_loc_to_xy(string_location: str):
    '''
    :param string_location: Coordinates in string form "X.Y" 
    :return: loc_x, loc_y as integers
    '''
    temp = string_location.split(".")
    try:
        loc_x = int(temp[0])
        loc_y = int(temp[1])
    except:
        loc_x = int(float(temp[0]))
        loc_y = int(float(temp[1]))

    return loc_x, loc_y