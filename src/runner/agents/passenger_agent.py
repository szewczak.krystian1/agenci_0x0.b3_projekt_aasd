import spade
from zmq.sugar.constants import QUEUE
from runner.agents.base_agent import BaseAgent
from spade.behaviour import CyclicBehaviour
from spade import agent, message
import typing
import protos.msg_pb2 as pb
import json
from asyncio import Queue
import janus
import datetime
from enum import Enum
from runner.agents.utils import distance_from_route, convert_string_loc_to_xy
import time
from threading import Lock


class PassengerAgent(BaseAgent, agent.Agent):

    def __init__(self, sock_addr, email, password) -> None:
        agent.Agent.__init__(self, email, password)
        self._offer_handling_behav = _OfferHandlingBehav()
        self._message_receiving_behav = _MessageReceivingBehav()
        self._request_checking_behav = _CheckUserRequestBehav()
        self._confirmation_handling_behav = _ConfirmationHandlingBehav()
        self._cancel_request_checking_behav = _CheckUserCancelRequest()
        self._cancel_driver_offer_handling_behav = _CancelDriverOfferHandlingBehav()
        BaseAgent.__init__(self, sock_addr)

        self._requests: typing.List[InnerRequest] = []

    async def setup(self):
        print('Agent %s created' % (self.name))
        self.add_behaviour(self._offer_handling_behav)
        self.add_behaviour(self._message_receiving_behav)
        self.add_behaviour(self._request_checking_behav)
        self.add_behaviour(self._confirmation_handling_behav)
        self.add_behaviour(self._cancel_request_checking_behav)
        self.add_behaviour(self._cancel_driver_offer_handling_behav)

    def _msg_type(self):
        return pb.PassengerMsg

    def _handlers(self) -> typing.Dict[type, typing.Callable]:
        return {
            'ping_req': self._handle_ping_req,
            'user_request_req': self._handle_request_req,
            'requests_status_req': self._handle_request_status_req,
            'cancel_request_req': self._handle_cancel_req,
        }

# HANDLERS
    def _handle_ping_req(self, event: pb.PingReq):
        self.reply_controller(pb.PassengerMsg(
            ping_res=pb.PingRes(seq=event.seq)))

    def _handle_request_req(self, event: pb.UserRequestReq):
        self._request_checking_behav._start_lock.acquire()
        self._request_checking_behav.request_queue.sync_q.put(
            event.request_info)
        self._request_checking_behav._start_lock.release()
        self.reply_controller(pb.PassengerMsg(
            user_request_res=pb.UserRequestRes()))

    def _handle_request_status_req(self, event: pb.RequestsStatusReq):
        status_proto_requests_list = []
        for request in self._requests:
            status_proto_requests_list.append(request.to_status_proto())
        reply = pb.PassengerMsg(
            requests_status_res=pb.RequestsStatusRes(requests=status_proto_requests_list))
        self.reply_controller(reply)

    def _handle_cancel_req(self, event: pb.CancelOfferReq):
        self._cancel_request_checking_behav._start_lock.acquire()
        self._cancel_request_checking_behav.cancel_queue.sync_q.put(
            event.index
        )
        self._cancel_request_checking_behav._start_lock.release()
        self.reply_controller(pb.PassengerMsg(
            cancel_request_res=pb.CancelRequestRes()
        ))


class _MessageReceivingBehav(CyclicBehaviour):

    async def on_start(self) -> None:
        print('Starting XMPP Message receiving behaviour')

    async def run(self) -> None:
        print('Waiting for XMPP message')
        msg = await self.receive(timeout=60)
        if msg is None:
            print('No message received')
            return
        print('Received XMPP message: ' + str(msg))
        await self.handle_msg(msg)

    async def on_end(self) -> None:
        print('Stopping XMPP Message receiving behaviour')

    async def handle_msg(self, msg: message.Message):
        m_body = msg.body
        try:
            b_dict: dict = json.loads(m_body)
            t = b_dict['msg_type']
            if t == 'offer':
                await self.agent._offer_handling_behav.offers_queue.put(msg)
            elif t == "confirmation":
                await self.agent._confirmation_handling_behav.confirms_queue.put(msg)
            elif t == "cancel":
                await self.agent._cancel_driver_offer_handling_behav.cancels_offers_queue.put(msg)
            else:
                raise Exception('Unknown message type')
        except Exception as e:
            print('Error parsing message: ' + str(e))


class _OfferHandlingBehav(CyclicBehaviour):

    async def on_start(self) -> None:
        print('Starting Offer Message handling receiving behaviour')
        self.offers_queue: "Queue[message.Message]" = Queue(maxsize=1000)

    async def run(self) -> None:
        print('Waiting for offers')
        offer_msg = await self.offers_queue.get()
        if offer_msg is None:
            return
        print('Received offer: ' + str(offer_msg))
        body = json.loads(offer_msg.body)
        request = await self.check_requests(offer_msg)
        if request is None:
            return
        await self._send_declaration(request, str(offer_msg.sender), body["offer_id"])

    async def on_end(self) -> None:
        print('Stopping Offer Message handling receiving behaviour')

    async def check_requests(self, offer_msg: message.Message):
        for request in self.agent._requests:
            if _does_offer_fits_request(offer=offer_msg, request=request):
                request.status = RequestStatus["Pending"]
                request.driver = str(offer_msg.sender)
                body = json.loads(offer_msg.body)
                request.offer = body["offer_id"]
                return request
        return

    async def _send_declaration(self, request: "InnerRequest", driver_name, offer_id):
        msg = message.Message(to=driver_name)
        msg.set_metadata('ontology', 'carpool')
        msg.set_metadata('perfomative', 'request')
        m_body = request.to_dict()
        m_body['msg_type'] = 'declaration'
        m_body['offer_id'] = offer_id
        msg.body = json.dumps(m_body)
        print('Sending msg: ' + str(msg))
        try:
            await self.send(msg)
        except Exception as e:
            print('Error sending msg: ' + str(e))
        print('Sent.')


class _CheckUserRequestBehav(CyclicBehaviour):

    def __init__(self):
        super().__init__()
        self._start_lock = Lock()
        self._start_lock.acquire()

    async def on_start(self) -> None:
        self.request_queue = janus.Queue()
        self._start_lock.release()
        print('Starting User Request Checking behaviour')

    async def run(self) -> None:
        print('Waiting for user request')
        new_request = await self.request_queue.async_q.get()
        if new_request is None:
            return
        print('New User Request:\n' + str(new_request))
        inner_request = InnerRequest(new_request)
        self.agent._requests.append(inner_request)

    async def on_end(self) -> None:
        print('Stopping Request Offer Checking behaviour')


class _ConfirmationHandlingBehav(CyclicBehaviour):

    async def on_start(self) -> None:
        print('Starting Confirmation Message handling behaviour')
        self.confirms_queue: "Queue[message.Message]" = Queue(maxsize=1000)

    async def run(self) -> None:
        print('Waiting for confirms')
        confirm_msg = await self.confirms_queue.get()
        if confirm_msg is None:
            return
        print('Received confirmation: ' + str(confirm_msg))
        await self.handle_confirmation(confirm_msg)

    async def handle_confirmation(self, msg: message.Message):
        body = json.loads(msg.body)
        conf = body['confirmation']
        req_idx = body['request_id']

        for request in self.agent._requests:
            if req_idx == request.index:
                if request.status != RequestStatus["Pending"]:
                    print('Status should be "Pending", but it is', request.status)
                if conf == "yes":
                    request.status = RequestStatus["Accepted"]
                elif conf == "no":
                    request.status = RequestStatus["Free"]
                    request.driver = None
                    request.offer = None
                else:
                    print("Problem with message body, wrong status!")

    async def on_end(self) -> None:
        print('Stopping Confirmation Message handling receiving behaviour')


class _CheckUserCancelRequest(CyclicBehaviour):

    def __init__(self):
        super().__init__()
        self._start_lock = Lock()
        self._start_lock.acquire()

    async def on_start(self) -> None:
        self.cancel_queue = janus.Queue()
        self._start_lock.release()
        print('Starting User Cancel Request Checking behaviour')

    async def run(self) -> None:
        print('Waiting for user request cancellation')
        new_cancellation = await self.cancel_queue.async_q.get()
        if new_cancellation is None:
            return
        print('New User Cancel Request:\n' + str(new_cancellation))
        request_index = new_cancellation
        await self.inform_driver(request_index)

    async def inform_driver(self, request_index):
        for req in self.agent._requests:
            if req.index == request_index:
                request = req
                break
            else:
                request = None
        if len(self.agent._requests) == 0:
            request = None

        if request is None:
            print("There is no such a request")
            return
        if request.status == RequestStatus["Free"]:
            print(
                "Request cancelled. No need to send information to driver, cause it was in 'Free' status.")
            self.agent._requests.remove(request)
            return

        msg = message.Message(to=request.driver)
        msg.set_metadata('ontology', 'carpool')
        msg.set_metadata('perfomative', 'cancel')
        m_body = {}
        m_body['driver'] = request.driver
        m_body['msg_type'] = 'cancel'
        m_body['offer_id'] = request.offer
        m_body['request_id'] = request_index
        msg.body = json.dumps(m_body)
        print('Sending msg: ' + str(msg))
        try:
            await self.send(msg)
        except Exception as e:
            print('Error sending msg: ' + str(e))
        print('Sent.')
        self.agent._requests.remove(request)

    async def on_end(self) -> None:
        print('Stopping User Cancel Checking behaviour')


class _CancelDriverOfferHandlingBehav(CyclicBehaviour):

    async def on_start(self) -> None:
        print('Starting Offer Cancel Message handling behaviour')
        self.cancels_offers_queue: "Queue[message.Message]" = Queue(
            maxsize=1000)

    async def run(self) -> None:
        print('Waiting for offer cancellations')
        cancel_msg = await self.cancels_offers_queue.get()
        if cancel_msg is None:
            return
        print('Received cancellation offer: ' + str(cancel_msg))
        await self.handle_cancellation(cancel_msg)

    async def handle_cancellation(self, msg: message.Message):
        # Check if req id and offer id in msg matches with ids in requests list
        # Set Request status to Free
        # Set Driver to None
        body = json.loads(msg.body)
        req_idx = body['request_id']
        offer_idx = body['offer_id']

        for request in self.agent._requests:
            if req_idx == request.index and offer_idx == request.offer:
                if request.status != RequestStatus["Accepted"]:
                    print('Status should be "Accepted", but it is', request.status)
                req = request
                break
            elif req_idx == request.index and offer_idx != request.offer:
                print("There was something wrong with set ID")
                req = request
                break
            else:
                req = None
        if req is None:
            print("Passenger does not have such a request, given by offer cancellation")
            return

        req.driver = None
        req.offer = None
        request.status = RequestStatus["Free"]

    async def on_end(self) -> None:
        print('Stopping Offer Cancel Message handling receiving behaviour')


class InnerRequest:
    cnt = 0

    def __init__(self, proto: pb.RequestInfo):
        # Proto data
        self.from_x = proto.from_loc.x_coord
        self.from_y = proto.from_loc.y_coord
        self.to_x = proto.to_loc.x_coord
        self.to_y = proto.to_loc.y_coord
        self.date = datetime.datetime.fromtimestamp(proto.date)
        # Business data
        self._driver = None
        self._offer = None
        self.index = self.cnt
        self.status = RequestStatus["Free"]
        InnerRequest.cnt += 1

    def to_status_proto(self) -> pb.RequestStatus:
        request_info_proto = pb.RequestStatus(
            request_info=pb.RequestInfo(
                from_loc=pb.Location(
                    x_coord=self.from_x,
                    y_coord=self.from_y
                ),
                to_loc=pb.Location(
                    x_coord=self.to_x,
                    y_coord=self.to_y
                ),
                date=int(time.mktime(self.date.timetuple()))
            ),
            driver_info=pb.UserInfo(
                name=self.driver
            ),
            index=self.index,
            status=self._request_status_to_proto(),
        )
        return request_info_proto

    def to_dict(self) -> dict:
        return {
            "request_id": self.index,
            'from': str(self.from_x) + '.' + str(self.from_y),
            'to': str(self.to_x) + '.' + str(self.to_y),
        }

    def _request_status_to_proto(self):
        if self.status == RequestStatus["Free"]:
            status = 1
        elif self.status == RequestStatus["Pending"]:
            status = 2
        else:
            status = 3
        return status

    @property
    def driver(self):
        return self._driver

    @driver.setter
    def driver(self, driver_name: str):
        self._driver = driver_name

    @property
    def offer(self):
        return self._offer

    @offer.setter
    def offer(self, offer_idx: int):
        self._offer = offer_idx


class RequestStatus(Enum):
    '''
    enum for setting request status
    Free - waiting for offers
    Pending - sent for driver to confirm
    Accepted  - accepted by driver
    '''
    Free = 1
    Pending = 2
    Accepted = 3


def _does_offer_fits_request(offer: message.Message, request: "InnerRequest"):
    # check if request Free
    if request.status != RequestStatus["Free"]:
        return
    # defining time_gap
    time_gap = 15 * 60  # 15 minutes.
    # get data from message
    offer_dict: dict = json.loads(offer.body)
    offer_from = offer_dict['from']
    offer_to = offer_dict['to']
    offer_date = datetime.datetime.fromtimestamp(float(offer_dict['date']))
    offer_offset = int(offer_dict['offset'])
    try:
        offer_offset = int(offer_dict['offset'])
    except:
        offer_offset = int(float(offer_dict['offset']))
    offer_from = convert_string_loc_to_xy(offer_from)
    offer_to = convert_string_loc_to_xy(offer_to)

    request_from = request.from_x, request.from_y
    request_to = request.to_x, request.to_y
    request_date = request.date

    # check data

    if abs((offer_date - request_date).total_seconds()) > time_gap:
        return False

    # check distance
    if distance_from_route(offer_from, offer_to, request_from, request_to) > offer_offset:
        return False

    return True
