from queue import Queue
import zmq
import threading
import abc
import typing
import traceback


class BaseAgent(abc.ABC):
    def __init__(self, sock_addr) -> None:
        self._queue = Queue()
        self._socket: zmq.Socket
        self._activate_socket(sock_addr)
        self._listen_thread = threading.Thread(target=self._listen)
        self._listen_thread.start()
        self._handler_thread = threading.Thread(target=self._handle_events)
        self._handler_thread.start()
        self._events_dict = self._handlers()

    def __enter__(self):
        print('Opening agent ' + str(type(self)))
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('Closing agent ' + str(type(self)))
        self._socket.close()  # TODO: send termination to listening thread other way
        self._listen_thread.join()
        self._queue.put(None)  # To terminate handler thread
        self._handler_thread.join()

    def _activate_socket(self, address):
        ctx = zmq.Context()
        self._socket = ctx.socket(zmq.PAIR)
        self._socket.bind('ipc:///tmp/%s' % (address))

    def _listen(self):
        while True:
            try:
                msg = self._socket.recv()  # TODO: close socket on end
                self._parse_msg(msg)
            except Exception as e:
                print('Receiving error: ' + str(e))
                break  # TODO: Break only on closed socket

    def _parse_msg(self, msg: str):
        msg_parsed = self._msg_type()()
        msg_parsed.ParseFromString(msg)
        print('Received message from controller:\n' + str(msg_parsed))
        field_name = msg_parsed.WhichOneof('content')
        self._queue.put((field_name, getattr(msg_parsed, field_name)))

    def _handle_events(self):
        while True:
            try:
                event = self._queue.get()
                if event is None:
                    break
                self._events_dict[event[0]](event[1])
            except KeyError:
                print('Event with no handler: ' + str(type(event)))

    def reply_controller(self, reply):
        print('Sending message to controller:\n' + str(reply))
        serial_msg = reply.SerializeToString()
        self._socket.send(serial_msg)

    @abc.abstractmethod
    def _msg_type(self):
        """Returns type of msg used for communication with controller"""
        pass

    @abc.abstractmethod
    def _handlers(self) -> typing.Dict[str, typing.Callable]:
        """Returns dict of handlers for specific events from msg type"""
        pass
