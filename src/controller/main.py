import protos.msg_pb2 as msg_pb2
import argparse
import zmq
import datetime
import time


def main():
    print("Running agent controller")
    p = make_parser()
    msg = None
    reply_parsed = None
    args = p.parse_args()
    if not args.subparser:
        p.print_help()
        exit(1)
    if args.subparser == 'driver':
        print('Controlling DRIVER')
        reply_parsed = msg_pb2.DriverMsg()

        if args.cmd == 'ping':
            msg = msg_pb2.DriverMsg(
                ping_req=msg_pb2.PingReq(seq=args.sequence))
        elif args.cmd == 'add_offer':
            msg = msg_pb2.DriverMsg(
                user_offer_req=msg_pb2.UserOfferReq(
                    offer_info=msg_pb2.OfferInfo(
                        from_loc=msg_pb2.Location(
                            x_coord=args.from_loc.x,
                            y_coord=args.from_loc.y,
                        ),
                        to_loc=msg_pb2.Location(
                            x_coord=args.to_loc.x,
                            y_coord=args.to_loc.y,
                        ),
                        places=args.max,
                        date=int(time.mktime(args.date.timetuple())),
                        max_offset=args.offset
                    )))
        elif args.cmd == 'show_offers':
            msg = msg_pb2.DriverMsg(
                offers_status_req=msg_pb2.OffersStatusReq()
            )
        elif args.cmd == 'cancel_offer':
            msg = msg_pb2.DriverMsg(
                cancel_offer_req=msg_pb2.CancelOfferReq(
                    index=args.index
                )
            )
        else:
            print('Unknown driver command')
            exit(1)

    elif args.subparser == 'passenger':
        print('Controlling PASSENGER')
        reply_parsed = msg_pb2.PassengerMsg()
        if args.cmd == 'ping':
            msg = msg_pb2.PassengerMsg(
                ping_req=msg_pb2.PingReq(seq=args.sequence))
        elif args.cmd == 'add_request':
            msg = msg_pb2.PassengerMsg(
                user_request_req=msg_pb2.UserRequestReq(
                    request_info=msg_pb2.RequestInfo(
                        from_loc=msg_pb2.Location(
                            x_coord=args.from_loc.x,
                            y_coord=args.from_loc.y,
                        ),
                        to_loc=msg_pb2.Location(
                            x_coord=args.to_loc.x,
                            y_coord=args.to_loc.y,
                        ),
                        date=int(time.mktime(args.date.timetuple()))
                    )))
        elif args.cmd == 'show_requests':
            msg = msg_pb2.PassengerMsg(
                requests_status_req=msg_pb2.RequestsStatusReq()
            )
        elif args.cmd == 'cancel_request':
            msg = msg_pb2.PassengerMsg(
                cancel_request_req=msg_pb2.CancelRequestReq(
                    index=args.index
                )
            )
        else:
            print('Unknown passenger command')
            exit(1)

    if msg is not None and reply_parsed is not None:
        sock = zmq.Context().socket(zmq.PAIR)
        sock.connect('ipc:///tmp/%s' % (args.socket))
        print('Sending message:\n' + str(msg))
        sock.send(msg.SerializeToString())
        reply = sock.recv()
        reply_parsed.ParseFromString(reply)
        print('Received message:\n' + str(reply_parsed))


def make_parser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(
        dest='subparser', help='Type of agent to communicate')
    parser.add_argument('-s', '--socket', dest='socket',
                        type=str, required=True, help='Socket address to communicate with agent')
    # DRIVER
    driver_parser = subparsers.add_parser(name='driver', help='Driver agent')
    driver_cmds = driver_parser.add_subparsers(dest='cmd')
    # -- PING
    driver_ping_parser = driver_cmds.add_parser(
        name='ping', help='Ping driver agent to check if alive')
    driver_ping_parser.add_argument(
        '-s', '--sequence', dest='sequence', type=int, required=True)
    # -- ADD OFFER
    driver_offer_parser = driver_cmds.add_parser(
        name='add_offer', help='Add new drive offer')
    driver_offer_parser.add_argument(
        '-f', '--from', dest='from_loc', type=Location, required=True, help='Start location in form XXX,YYY, where XXX and YYY are coordinates')
    driver_offer_parser.add_argument(
        '-t', '--to', dest='to_loc', type=Location, required=True, help='End location in form XXX,YYY, where XXX and YYY are coordinates')
    driver_offer_parser.add_argument(
        '-m', '--max', dest='max', type=int, required=True, help='Maks number of passengers')
    driver_offer_parser.add_argument(
        '-d', '--date', dest='date', type=lambda s: datetime.datetime.strptime(s, '%Y_%m_%d_%H_%M'),
        required=True, help='Date and time of start in form YYYY_mm_dd_HH_MM')
    driver_offer_parser.add_argument(
        '-o', '--offset', dest='offset', type=int, required=True, help='Maks offset from route')
    # -- SHOW OFFERS
    driver_show_offers_parser = driver_cmds.add_parser(
        name='show_offers', help='Shows my current offers')
    # -- CANCEL OFFER
    driver_cancel_offer_parser = driver_cmds.add_parser(
        name='cancel_offer', help='Cancel selected offer')
    driver_cancel_offer_parser.add_argument(
        '-i', '--index', dest='index', type=int, required=True, help='Index of offer to cancel')

    # PASSENGER
    passenger_parser = subparsers.add_parser(
        name='passenger', help='Passenger agent')
    passenger_cmds = passenger_parser.add_subparsers(dest='cmd')
    # -- PING
    passenger_ping_parser = passenger_cmds.add_parser(
        name='ping', help='Ping passenger agent to check if alive')
    passenger_ping_parser.add_argument(
        '-s', '--sequence', dest='sequence', type=int, required=True)
    # -- ADD REQUEST
    passenger_request_parser = passenger_cmds.add_parser(
        name='add_request', help='Add new drive request')
    passenger_request_parser.add_argument(
        '-f', '--from', dest='from_loc', type=Location, required=True, help='Start location in form XXX,YYY, where XXX and YYY are coordinates')
    passenger_request_parser.add_argument(
        '-t', '--to', dest='to_loc', type=Location, required=True, help='End location in form XXX,YYY, where XXX and YYY are coordinates')
    passenger_request_parser.add_argument(
        '-d', '--date', dest='date', type=lambda s: datetime.datetime.strptime(s, '%Y_%m_%d_%H_%M'),
        required=True, help='Date and time of start in form YYYY_mm_dd_HH_MM')
    # -- SHOW REQUESTS
    passenger_show_requests_parser = passenger_cmds.add_parser(
        name='show_requests', help='Shows my current requests')
    # -- CANCEL OFFER
    passenger_cancel_request_parser = passenger_cmds.add_parser(
        name='cancel_request', help='Cancel selected request')
    passenger_cancel_request_parser.add_argument(
        '-i', '--index', dest='index', type=int, required=True, help='Index of request to cancel')

    return parser


if __name__ == '__main__':
    main()


class Location:
    def __init__(self, s: str):
        try:
            self.x = int(s.split(',')[0])
            self.y = int(s.split(',')[1])
        except Exception as e:
            raise Exception('Error parsing location: ' + str(e))
