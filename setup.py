from setuptools import find_packages, setup

requrements = []
with open('requirements.txt', 'r') as r:
    for line in r.readlines():
        if line.strip().startswith('#'):
            continue
        requrements.append(line.strip())

setup(
    name='AASD',
    version='0.0.1',
    #packages=['controller', 'runner', 'protos'],
    packages=find_packages('src'),
    package_dir={'': '.', 'controller': 'src/controller',
                 'runner': 'src/runner', 'protos': 'src/protos'},
    install_requires=requrements,
    entry_points={
        'console_scripts': ['runner=runner.main:main',
                            'controller=controller.main:main']
    }
)
