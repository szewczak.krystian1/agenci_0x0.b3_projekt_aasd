# Agenci_0x0.B3_projekt_AASD

# Instrukcja (dla Linuxa, na Windowsa nie mamy licencji...)
1. Należy mieć python3.8, python3.8-venv
2. Należy uruchomić skrypt _prepare-env.sh_
3. Należy aktywować _venv_ poleceniem _source venv/bin/activate_
4. ~~Należy dociągnąć _pip-tools_, skompilować _requirements.in_ i zsynchronizować _requirements.txt_~~
5. Żartowałem z powyższym, znacie mnie z prostych przepsiów - wystarczy wywołać skrypt _prepare-deps.sh_

# Uwagi
- Do komunikacji kontrolera z agentem w obrębie maszyny użyte są protobufy
- Ich kompilator należy instalować zgodnie z [linkiem](https://askubuntu.com/questions/1072683/how-can-i-install-protoc-on-ubuntu-16-04)
- Protosa kompiluje się skryptem _update_protos.sh_
